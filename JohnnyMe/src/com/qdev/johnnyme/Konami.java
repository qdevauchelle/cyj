package com.qdev.johnnyme;

import android.app.Activity;
<<<<<<< HEAD
import android.app.Service;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class Konami extends SimpleOnGestureListener  {
	
	int codeCounter = 0;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private int width;
    private int height;
    		
    
    public interface OnKonamiListener{
    	public void onKonamiDone();	
    	public void onKonamiDone(String txt);
    }
    
    private OnKonamiListener mListener = null;
    
    public void setOnKonamiListener(OnKonamiListener listener){
    	mListener = listener;
    }
    
    
    /**
     * Constructor
     * @param activity
     */
    public Konami(Activity activity, int width, int height){
    	setOnKonamiListener((OnKonamiListener) activity);
    	this.width = width;
    	this.height = height;    	
    }
    
    public Konami(Service service, int width, int height){
    	setOnKonamiListener((OnKonamiListener) service);
=======
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class Konami extends SimpleOnGestureListener  {
	
	int codeCounter = 0;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private int width;
    private int height;
    		
    
    public interface OnKonamiListener{
    	public void onKonamiDone();	
    	public void onKonamiDone(String txt);
    }
    
    private OnKonamiListener mListener = null;
    
    public void setOnKonamiListener(OnKonamiListener listener){
    	mListener = listener;
    }
    
    
    /**
     * Constructor
     * @param activity
     */
    public Konami(Activity activity, int width, int height){
    	setOnKonamiListener((OnKonamiListener) activity);
>>>>>>> refs/remotes/origin/master
    	this.width = width;
    	this.height = height;    	
    }

    
    @Override
    public boolean onSingleTapConfirmed (MotionEvent e){
    	if(e.getX() < (width/2) ){
    		//tap left
    		incrementCode(5);
    	} else{
    		//tap right
    		incrementCode(6);
    	}
		return false;
    }
    
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {

		    // down to up swipe
			if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY){
            	if (Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH){
            		return false;
            	}
            	incrementCode(1);
            	
			// up to down swipe
			}else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY){
            	if (Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH)
                    return false;
            	incrementCode(2);
            	
            // right to left swipe
            }else if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
            	incrementCode(3);
            	
            // left to right swipe
            }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            	if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
            	incrementCode(4);
            }
			
        return false;
	}
	
	
	/**
	 * increment the code counter according to the gesture
	 * @param code (code = 1 : down to up, 2 : up to down, 3 : right to left, 4 : left to right, 5 : tap left, 6 : tap right)
	 */
	private void incrementCode(int code){
		switch (code) {
		case 1:
			if (codeCounter == 0 || codeCounter == 1){
				codeCounter++;
			}else{
				codeCounter = 0;
			}
			break;
			
		case 2:
			if (codeCounter == 2 || codeCounter == 3){
				codeCounter++;
			}else{
				codeCounter = 0;
			}
			break;
			
		case 3:
			if (codeCounter == 4 || codeCounter == 6){
				codeCounter++;
			}else{
				codeCounter = 0;
			}
			break;
			
		case 4:
			if (codeCounter == 5 || codeCounter == 7){
				codeCounter++;
			}else{
				codeCounter = 0;
			}
			break;
			
		case 5:
			if (codeCounter == 8){
				codeCounter++;
			}else{
				codeCounter = 0;
			}
			break;
			
		case 6:
			if (codeCounter == 9){
				codeCounter++;
				//THE CODE IS COMPLETE!
				mListener.onKonamiDone();
			}else{
				codeCounter = 0;
			}
			break;

		default:
			break;
		}
//		mListener.onKonamiDone("test"+codeCounter);
	}
}
