package com.qdev.johnnyme;


import com.qdev.johnnyme.Konami.OnKonamiListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener, OnKonamiListener{
    private GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;
    TextView tv = null;
	
	@SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        RelativeLayout view = (RelativeLayout) getLayoutInflater().inflate(R.layout.main_activity, null);
        tv = (TextView) view.findViewById(R.id.textView1);
        
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        
        int width, height;
        if (android.os.Build.VERSION.SDK_INT >= 13){
        	display.getSize(size);
        	width = size.x;
            height = size.y;
        }else{
        	width = display.getWidth();
        	height = display.getHeight();
        }
        
        tv.setText("test");

        
        //Instantiate Konami 
        Konami konami = new Konami(this, width, height);


        // Gesture detection
        gestureDetector = new GestureDetector(this, konami);
        gestureListener = new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
        };
        
        view.setOnClickListener((OnClickListener) MainActivity.this);
        view.setOnTouchListener(gestureListener);
        setContentView(view);
        
    }


	@Override
	public void onKonamiDone() {
				tv.setText("OK");
	}

	@Override
	public void onKonamiDone(String txt) {
		tv.setText(txt);
		
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}


}
<<<<<<< HEAD



=======
>>>>>>> refs/remotes/origin/master
