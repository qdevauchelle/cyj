import os, glob, fnmatch, re, shutil, sys


"""
A python script that treats an android source directory to add a Konami code that spawns Johnny's picture
"""





path = """C:\Users\QT\workspace\dummy"""

#Change the path to the right value if set as an argument
path = raw_input("Please enter your app\'s directory")
if path == "":
    path = """C:\Users\QT\workspace\dummy"""

packagename = ""
activityname = ""
layoutname = ""
srcpath = ""
imgpath = path+"\\res\\drawable"
layoutpath = path+"\\res\\layout"



#get the main activity name
for r,d,f in os.walk(path):
    for files in f:
        if fnmatch.fnmatch(files, "*ctivity*.java"):
            ok = raw_input("Do you want to patch "+files+".java ? (y/n)")
            if ok in {'y', 'Y'}:
                activityname = files.split('.java')[0]
                srcpath = os.path.join(r,"")
                break
            
        

#Get the package name and then the main layout according to this activity's "setcontentview"
with open(srcpath+"\\"+activityname+".java", 'r+b') as f:
    for lines in f:
        matchPackage = re.search(r"package \w+[\.\w]*", lines)
        if matchPackage:
            packagename = re.sub("package ","",matchPackage.group(),count=1)
            break
    
    for lines in f:                
        matchLayout = re.match(r".*setContentView\(.*\);", lines)
        if matchLayout:
            layoutname = re.sub(".*setContentView\(","",matchLayout.group(),count=1)
            layoutname = layoutname.split(');')[0]
            break

#populate a table with data to escape
def populate(table):
    with open("ToAddToOnCreate.txt",'rb') as f:
                    for lines in f:
                        table.append(re.compile(lines))
                    f.close()
    with open("MethodsToAdd.txt",'rb') as f:
                    for lines in f:
                        table.append(re.compile(lines))
                    f.close()
    table.append(re.compile("\s*\t*import "+packagename+".Konami.OnKonamiListener;\n"))
    table.append(re.compile("\s*\t*package.*"))
    table.append(re.compile("\s*\t*private GestureDetector gestureDetector;"))
    table.append(re.compile("\s*\t*View.OnTouchListener gestureListener;"))
    table.append(re.compile("import android.graphics.Point;"))
    table.append(re.compile("import android.view.Display;"))
    table.append(re.compile("import android.view.GestureDetector;"))
    table.append(re.compile("import android.view.MotionEvent;"))
    table.append(re.compile("import android.view.View;"))
    table.append(re.compile("import android.view.View.OnClickListener;"))
    return table


#rewrite class declaration    
def rewrite_class_declaration(declaration):
    declarationtype = 0

    if re.match(r"\s*\t*\w*\s*class\s[\w\s]*\simplements.*", declaration):
        implement = re.sub(r"\s*\t*\w*\s*class\s[\w\s]*implements","",declaration,count=1)
        declaration = re.sub(r" implements.*$","",declaration,count=1)
        if "OnClickListener" not in implement:
            implement = implement+", OnClickListener"
        if "OnKonamiListener{\n" not in implement:
            implement = implement+", OnKonamiListener{\n"
        declarationtype = declarationtype + 1
    if re.match(r"\s*\t*\w*\s*class\s[\w\s]*\sextends.*", declaration):
        extend = re.sub(r"\s*\t*\w*\s*class\s[\w\s]*\sextends\s","",declaration,count=1)
        extend = extend.replace(' {','')
        declaration = re.sub(r" extends.*$","",declaration,count=1)
        
        declarationtype= declarationtype + 2
        #print "declaration temp : "+declaration+" declarationtype :"+str(declarationtype)+" extend :"+extend
    
    if declarationtype == 3:
        declaration = declaration+" extends "+extend+" implements "+implement+"{\n"
    elif declarationtype == 2:
        declaration = declaration+" extends "+extend+" implements OnClickListener, OnKonamiListener{\n"
    elif declarationtype == 1:
        declaration = declaration+" implement "+implement+"{\n"
    else:
        declaration = re.sub(r"{$","",declaration,count=1)
        declaration = declaration+" implements OnClickListener, OnKonamiListener{\n"
    
    declaration = "\n\n@SuppressLint(\"NewApi\")\n"+declaration
    return declaration





#copy a class file to the new directory
def copy_class(classname):
    if not hasattr(classname+".java", 'read'):
            source = open(classname+".java", 'rb')
    if not hasattr(srcpath+"\\"+classname+".java", 'write'):
            dest = open(srcpath+"\\"+classname+".java", 'wb')
            
    dest.write("package "+packagename+";\n\n")
    for line in source:
        dest.write(line)
        
    source.close()
    dest.close()



#populates the escape table
regexes = []
regexes = populate(regexes)

#Start to write
if not hasattr(srcpath+"\\"+activityname+".java", 'read'):
        source = open(srcpath+"\\"+activityname+".java", 'rb')
if not hasattr(srcpath+"\\"+activityname+".java"+"new", 'write'):
        dest = open(srcpath+"\\"+activityname+".java"+"new", 'wb')
        
dest.write("package "+packagename+";\n\n")
dest.write("import "+packagename+".Konami.OnKonamiListener;\n")
dest.write("import android.graphics.Point;\n")
dest.write("import android.view.Display;\n")
dest.write("import android.view.GestureDetector;\n")
dest.write("import android.view.MotionEvent;\n")
dest.write("import android.view.View;\n")
dest.write("import android.view.View.OnClickListener;\n")
dest.write("import android.annotation.SuppressLint;\n")
dest.write("import android.content.Intent;\n")








for line in source:
    if not any(regex.match(line) for regex in regexes):

        if re.match("\s*\t*[\w\s]*class\s"+activityname+".*\{",line):#rewrites the class declaration
                line = rewrite_class_declaration(line)
                dest.write(line)
                dest.write("\tprivate GestureDetector gestureDetector;\n")
                dest.write("\tView.OnTouchListener gestureListener;\n")
        elif re.match("\s*\t*[\w\.]*setContentView.*;",line):
                with open("ToAddToOnCreate.txt",'rb') as f:
                    for lines in f:
                        dest.write(lines)
                    f.close()
                dest.write(layoutname+".setOnClickListener((OnClickListener) "+activityname+".this);\n");
                dest.write("\t"+layoutname+".setOnTouchListener(gestureListener);\n")
                dest.write(line+"\t}\n")
        elif re.match("}", line):#Writes the added methods before the end of the class
                dest.write("\n}\n")
                with open("MethodsToAdd.txt",'rb') as f:
                    for lines in f:
                        dest.write(lines)
                    f.close
                dest.write("}")
                break
        else:
                dest.write(line)
        
dest.close()
source.close()

filelist = glob.glob(srcpath+"\\"+activityname+".java")
for f in filelist:
    os.remove(srcpath+"\\"+activityname+".java")
    os.rename(srcpath+"\\"+activityname+".javanew", srcpath+"\\"+activityname+".java")
    
    
#copy the konami class    
copy_class("Konami");

#copy the JohnnyActivity class
copy_class("JohnnyActivity");

#copy the johnny.xml
if not os.path.isdir(layoutpath+"\\"):
    os.makedirs(layoutpath)
shutil.copy("johnny.xml",layoutpath+"\\johnny.xml")

#copy the johnny.png drawable
if not os.path.isdir(imgpath+"\\"):
    os.makedirs(imgpath)
shutil.copy("johnny.png",imgpath+"\\johnny.png")


#modify the manifest to include the new activity
manifestfile = path+"\\AndroidManifest.xml"
if not hasattr(manifestfile, 'read'):
            source = open(manifestfile, 'rb')
if not hasattr(manifestfile+".new", 'write'):
            dest = open(manifestfile+".new", 'wb')
            
for line in source:
    if not re.match(r"\s*\t*.*Johnny.*",line):
        if not re.match(r"\s*\t*.*/activity.*", line):
            dest.write(line)
        else:
            dest.write(line+"\n\t\t<activity android:name=\""+packagename+".JohnnyActivity\"\nandroid:label=\"@string/app_name\"></activity>\n")

source.close()
dest.close()

os.remove(manifestfile)
os.rename(manifestfile+".new", manifestfile)

raw_input ("The script has finished its job. Your app is now ready. Compile it and go test that Konami code ;)")
sys.exit()