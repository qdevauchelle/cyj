


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * A popup with the image of Johnny
 * @author QT
 *
 */
public class JohnnyActivity extends Activity {
	
	@Override
	 protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        RelativeLayout view = (RelativeLayout) getLayoutInflater().inflate(R.layout.johnny, null);
        ImageView iv = (ImageView) view.findViewById(R.id.johnny_image);
        iv.setImageResource(R.drawable.johnny);
        view.setBackgroundColor(Color.BLACK);
        setContentView(view);
	}
	

}
